import {Container} from 'react-bootstrap'
import {Fragment, useEffect, useState} from 'react'
import './App.css';
import NavBar from './components/NavBar'
import Footer from './components/Footer'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './Pages/Home'
import Tops from './Pages/Tops'
import Kicks from './Pages/Kicks'
import Pants from './Pages/Pants'
import Accesories from './Pages/Ace'
import Login from './Pages/Login'
import Logout from './Pages/Logout'
import Register from './Pages/Register'
import Products from './Pages/Products'
import SpecificProduct from './Pages/SpecificProduct'
import Orders from './Pages/Orders'



import {UserProvider} from './UserContext';
  

function App(){

   
   const[user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true',
    _id: localStorage.getItem('_id')
    
  })

      const unsetUser = () => {

      localStorage.clear()
  }

   useEffect(() => {
      console.log(user);
      console.log(localStorage)

    }, [user])

return( 
  
   <UserProvider value={ {user,setUser,unsetUser} }>
  
  <div className="page-container">
  <div className="content-wrap">
  <Router>
  
  <NavBar /> 


  <Switch>
      < Route exact path="/" component={Home} />
      < Route exact path="/products" component={Products} />
      < Route exact path="/products/:productId" component={SpecificProduct}/>
      < Route exact path="/Tops" component={Tops} />
      < Route exact path="/Pants" component={Pants} />
      < Route exact path="/Shoes" component={Kicks} />
      < Route exact path="/accesories" component={Accesories} />
      < Route exact path="/login" component={Login} />
      < Route exact path="/logout" component={Logout} />
      < Route exact path="/register" component={Register} />
      < Route exact path="/user/:userId" component={Orders} />

  
  
  </Switch>
 
 
  </Router>

  </div>

  <Footer />
  
  </div>
  </UserProvider>

  )

}


export default App;