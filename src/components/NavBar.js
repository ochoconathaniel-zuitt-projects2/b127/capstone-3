
import { React , useContext, Fragment} from 'react'
import UserContext from '../UserContext'
import {AiOutlineShopping} from 'react-icons/ai'

import { Nav, NavLink , Bars , NavMenu , NavBtn , NavBtnLink } from './NavBarElements'


const NavBar = () => {

  const { user } = useContext(UserContext);
   

    let rightSection = (user.accessToken !== null) ?
         <Fragment> 
         <NavMenu>
          <NavBtnLink className="btn-lg" to={`/user/${user._id}`} activeStyle>
         <AiOutlineShopping />
         </NavBtnLink>
         <NavBtnLink to = "/products" activeStyle>
         All Products
         </NavBtnLink>
         <NavBtnLink to = "/logout" activeStyle>
         Log out
         </NavBtnLink>
         </NavMenu>
         </Fragment>  
        
         :

         <Fragment>
         <NavMenu>
         <NavBtnLink to = "/register" activeStyle>
         Register
         </NavBtnLink>
         <NavBtnLink to = "/login" activeStyle>
         Log in
         </NavBtnLink>
         </NavMenu>
         </Fragment>


  return (
      
      
    

     <> 
      <Nav> 
        <NavLink to = "/">
        <h1>kyoShiro</h1>
        </NavLink>
        <Bars />
         {rightSection}
         </Nav>

       
      </>
    )


}




export default NavBar;