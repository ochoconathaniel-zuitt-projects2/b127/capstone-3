import { useState, useEffect, Fragment} from 'react';
import ShoesCard from './ShoesCard';
import {Col, Row} from 'reactstrap'

export default function ShoesView ({ shoeData }){

	const [shoes,setShoes] = useState([])


	useEffect(()=> {

			const shoesArr = shoeData.map(kicks => {

					if(kicks.isActive === true){

						return(
							< ShoesCard shoeProp={kicks} key={kicks._id}/>

							)

					} else {

						return null;
					}




			})


				setShoes(shoesArr)


	},[shoeData])

  	return(
  		<Fragment>


  		<Row xs={12} md={3} lg={3} >
  			
  			{shoes}
  		
  		</Row>

  		</Fragment>	


  		)


}