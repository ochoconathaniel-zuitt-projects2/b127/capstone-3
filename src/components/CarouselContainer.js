import React from 'react';
import { Carousel } from 'react-bootstrap';
import image1 from '../images/sw1.jpg'
import image2 from '../images/sw5.jpg'
import image3 from '../images/sw6.jpg'

const CarouselContainer = () => {

	return (

		<Carousel fade>
    <Carousel.Item interval={5000}>
    <img
      className="d-block w-100"
      src={image1}
      alt="First slide"
    />
   
    </Carousel.Item>
    <Carousel.Item interval={5000}>
    <img
      className="d-block w-100"
      src={image2}
      alt="Second slide"
     />

    

    </Carousel.Item>
    <Carousel.Item>
    <img
      className="d-block w-100"
      src={image3}
      alt="Third slide"
     />
  
     </Carousel.Item>
     </Carousel>

  

		)
} 

export default CarouselContainer;