import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import TopView from '../components/TopView';
import UserContext from '../UserContext';

export default function Tops() {
	
	const { user } = useContext(UserContext);

	const [allTops, setAllTops] = useState([])

	const fetchShirts = () => {
		fetch('http://localhost:4000/product/')
		.then(res => res.json())
		.then(data =>{
			
			setAllTops(data)
		
		})
	}

	useEffect(()=>{
		fetchShirts()
	}, [])

	return(
		
		<Container>
			{
			
			 
			 	<TopView shirtData={allTops}/>
			}
		</Container>
		
		)
}