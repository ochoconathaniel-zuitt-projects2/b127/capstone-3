import { useState , useEffect, useContext} from 'react';
import { Container, Card , Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, useParams } from 'react-router-dom';
import '../components/Specific.css'

import UserContext from '../UserContext';

export default function SpecificProduct(){
		const {user} = useContext(UserContext);		


		const [ productName, setName] = useState('')
		const [ productDescription,setDescription ] = useState('')
		const [ productPrice, setPrice ] = useState(0)
		const [ ProfilePicture , setPicture] = useState('')



		
	

		const { productId } = useParams();



		useEffect(() => {

			fetch(` http://localhost:4000/product/${productId}`)
			.then(res => res.json())
			.then(data => {
				setName(data.productName)
				setDescription(data.productDescription)
				setPrice(data.productPrice)
				setPicture(data.ProfilePicture)
			})
		},[])


		const order = () => {
			fetch( 'http://localhost:4000/user/placeOrder', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${ localStorage.getItem('accessToken')}`

				},
				body:JSON.stringify({
					productId: productId
					
					
					
				})
			})

			.then(res => res.json())
			.then(data => {
				if (data === true){



					Swal.fire({
						title: 'Arigato!',
						icon: 'success',
						text: `Your order "${ productName }" is placed`,
						confirmButtonColor: 'black'
					})
				} else {

					Swal.fire({
						title: "ooops",
						icon: 'error',
						text: 'something went wrong'
					})

				}




			})
		}


		return (

		<Container>
		

			<div className="app">	
				
				<div className="details">
					<div className="big-img">
						<img src={`http://localhost:4000/uploads/${ProfilePicture}`}/>
					</div>

					<div className="Sbox">
					<div className="row">
					  <h2>{productName}</h2>
					  <span>{productPrice}</span>
					 </div>
					 
					<p>{productDescription}</p>

				




					{
						user.accessToken !== null && user.isAdmin !== true ?
							<button className="cart"  block onClick={() => order(productId)}>Purchase</button>
							:
							<Link className="btn btn-warning btn-block" to="/login">Login to place an order
							</Link>


					}








					 </div>
					</div>




				</div>




	 	</Container>

			 	)

		
}