import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';







export default function Logout() {

	
	const { setUser, unsetUser } = useContext(UserContext)

	

	useEffect(() => {
		setUser({accessToken:null})

		}, [])

	
	return(


		Swal.fire({
 		 title: 'Are you sure?',
 		 text: "Leaving so soon?",
 		 icon: 'warning',
 		 showCancelButton: true,
  		 confirmButtonColor: 'black',
 		 cancelButtonColor: 'black',
 		 confirmButtonText: 'Yes, Log out'
		 }).then((result) => {
  		 if (result.isConfirmed) {
    		unsetUser();
    		window.location = "/login";	
 		 } else {
 		 	window.location = "/";
 		 }

		})

		 

		




		)

}