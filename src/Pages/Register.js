import { Fragment, useState, useEffect, useContext } from 'react';
import '../components/Register.css'
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';




export default function Register(){

		const { user } = useContext(UserContext);

		const history = useHistory();

		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [password2, setPassword2] = useState('');
		const [address, setAddress] = useState('')

		const [isActive, setIsActive] = useState(false);


		useEffect(()=>{
	
		if((firstName !== '' && lastName !== '' && email !== '' && password !== '' && password2 !== '' && address !== '' ) && (password === password2))
		{
			setIsActive(true);
		}else{
			setIsActive(false);
		}

		}, [firstName, lastName, email, password, password2 , address])


		function registerUser(e){
		
		e.preventDefault();

		
	
		fetch('http://localhost:4000/user/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Email is already registered!',
					icon: 'error',
					text: 'Try using another email to register',
					confirmButtonColor: 'black'
				})
			}else{
					fetch ('http://localhost:4000/user/register', {
					method: 'POST',
					headers: {
					'Content-Type': 'application/json'
					},
			
						body: JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						email:email,
						password:password,
						address:address
					})
			})
					.then(res => res.json())
					.then(data => {
				
						if(data === true){
					

						Swal.fire({

						title: 'Welcome',
						icon: 'success',
						text: 'Registration Successful!',
						confirmButtonColor: 'black'



						});

						history.push('/login')
						



						} 





					else {

						Swal.fire({

						title: 'oops!',
						icon: 'error',
						text: 'something went wrong'


							});

						}

					})


			}






		})

		
						setFirstName('')
						setLastName('')
						setEmail('')
						setPassword('')
						setPassword2('')	
						setAddress('')
			
		
	}






	return (

		<Fragment>
		<body className="Register">
		
		  <form className="Rbox" onSubmit={(e) => registerUser(e)}>
		  
		  <h1>Register</h1>
		  <input type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required></input>
		  <input type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required></input>
		  <input type="text" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} required></input>
		  <input type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required></input>
		  <input type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}required></input>
		  <input type="text" placeholder="Address" value={address} onChange={e => setAddress(e.target.value)} required></input>
		  
		  { isActive ?
		  <Button type="submit">Submit</Button>	
			:
		  <Button type="submit" disabled>Submit</Button>	
		  }	
		  </form>	
		

		</body>

		</Fragment>

		)
}		