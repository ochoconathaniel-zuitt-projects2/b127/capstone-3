import { useState , useEffect, useContext} from 'react';
import { Container, Card , Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, useParams } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Orders(){
			
		const {user} = useContext(UserContext);		


		const [ productId, setId] = useState('')
		const [ purchaseDate, setDate ] = useState(0)
		const [ status, setStatus ] = useState(0)


		const { userId } = useParams();



		useEffect(() => {

			fetch(`http://localhost:4000/user/${userId}/orders`)
			.then(res => res.json())
			.then(data => {
				setId(data.orders.productId)
				setDate(data.orders.purchasedOn)
				setStatus(data.orders.status)

			})
		},[])



		return (

		<Container>
			<Card>
			 <Card.Header className="bg-dark text-white text-center pb-0">
			 	<h4>Orders</h4>
			 	<h4>{productId}</h4>
			 </Card.Header>

			 <Card.Body>
			 	<Card.Text className="text-center">
			 	</Card.Text>
			 
				
			 <h6 className="text-center">{purchaseDate}</h6>
			 <h6 className="text-center">{status}</h6>


			 	</Card.Body>

			 	
			 		
			

			 	</Card>
			 	</Container>
			 	)

		
}